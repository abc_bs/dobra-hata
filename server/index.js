/**
 * Created by Home-PC on 09.04.2016.
 */

var exec = require('child_process').exec,
    express = require('express'),
    app = express();

const PORT = process.env.PORT || 3000;

app.use(express.static('../'));

app.listen(PORT, function(){
    console.log('Project started on port', PORT);
    exec('start http://localhost:' + PORT);
});