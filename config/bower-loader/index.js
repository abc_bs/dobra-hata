/**
 * Created by Home-PC on 09.04.2016.
 */

var exec = require('child_process').exec,
    fs = require('fs');

function loadPackageJSON(){
    var prBowerPack = new Promise(function(resolve, reject){
        console.log('- Loading Bower package.json');

        fs.readFile('bower-packages.json', 'utf8', function (err, data) {
            if (err) {
                throw err;
                reject(err);
                console.log('- Error: Fail to upload package.json');
            } else{
                resolve(JSON.parse(data));
                console.log('- Success loading Bower package.json');
            }
        });
    });

    return prBowerPack;
}

function loadPackages(data){
    var promises = [],
        packages = data.dependencies;

    console.log('- Started loading Bower packages');
    packages.forEach(function(pack){
        var promise = new Promise(function(resolve, reject){
            var cmd = 'bower install ' + pack.lib + '=' + pack.name + '#' + pack.version;
            console.log('-- Started loading <', pack.name.toUpperCase(), '> package', cmd);
            exec(cmd, function(error, stdout, stderr) {
                if(error){
                    throw error;
                    reject(error);
                } else{
                    resolve();
                    console.log('-- Finished loading <', pack.name.toUpperCase(), '> package');
                }
            });
        });

        promises.push(promise);
    });

    return Promise.all(promises);
}

exports.loadPackagesData = loadPackageJSON;
exports.loadPackages = loadPackages;