/**
 * Created by Home-PC on 09.04.2016.
 */
var exec = require('child_process').exec,
    fs = require('fs');

function loadPackageJSON(){
    var prNPMPack = new Promise(function(resolve, reject){
        console.log('- Loading NPM package.json');

        fs.readFile('npm-packages.json', 'utf8', function (err, data) {
            if (err) {
                throw err;
                reject(err);
                console.log('- Error: Fail to upload package.json');
            } else{
                resolve(JSON.parse(data));
                console.log('- Success loading NPM package.json');
            }
        });
    });

    return prNPMPack;
}
function loadPackages(data){
    var promises = [],
        packages = data.packages;

    console.log('- Started loading packages');
    packages.forEach(function(pack){
        var promise = new Promise(function(resolve, reject){
            var cmd = 'npm install --prefix ../ ' + pack;

            console.log('-- Started loading <', pack.toUpperCase(), '> package');

            exec(cmd, function(error, stdout, stderr) {
                if(error){
                    throw error;
                    reject(error);
                } else{
                    resolve();
                    console.log('-- Finished loading <', pack.toUpperCase(), '> package');
                }
            });
        });

        promises.push(promise);
    });

    Promise.all(promises).then(function(){ console.log('- Finished loading packages'); }, function(){});

    return Promise.all(promises);
}

exports.loadPackagesData = loadPackageJSON;
exports.loadPackages = loadPackages;