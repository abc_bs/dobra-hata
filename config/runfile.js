/**
 * Created by Home-PC on 09.04.2016.
 */
var exec = require('child_process').exec;

var npm_loader = require('./npm-loader');
var bower_loader = require('./bower-loader');

npm_loader.loadPackagesData().then(function(data){
    npm_loader.loadPackages(data).then(function(){
        bower_loader.loadPackagesData().then(function(data){
            bower_loader.loadPackages(data).then(function(){
                exec('node ../server/index', function(err){
                    if(err) throw err;
                });
            }, function(){});
        }, function(){});
    }, function(){});
}, function(){});