/**
 * Created by Home-PC on 09.04.2016.
 */

require.config({

    //  псевдонимы и пути используемых библиотек и плагинов
    paths: {
        'domReady': '../libs/domready/domReady',
        'angular': '../libs/angular/angular',
        'angular-route': '../libs/angular-route/angular-route'
    },

    // angular не поддерживает AMD из коробки, поэтому экспортируем перменную angular в глобальную область
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        }
    },

    // запустить приложение
    deps: ['./bootstrap']
});