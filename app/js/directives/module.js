/**
 * Created by Home-PC on 09.04.2016.
 */

define(['angular'], function (ng) {
    'use strict';
    return ng.module('app.directives', []);
});