/**
 * Created by Home-PC on 09.04.2016.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: '../app/templates/home.html',
            controller: 'mainCtrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }]);
});